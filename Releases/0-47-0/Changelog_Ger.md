Ein weiteres Jahr geschäftigen Treibens und erfolgreicher Entwicklungsarbeit liegt hinter uns - und wir, das OpenMW-Team, sind stolz darauf, die Veröffentlichung der Version 0.47.0 unserer Open-Source-Engine verkünden zu können! Ladet Euch die aktuelle Version auf unserer (englischsprachigen) [Download-Seite](https://openmw.org/downloads/) für Windows, Linux oder macOS herunter.

Mit über 180 bearbeiteten Aufgaben und einer Fülle an neuen Features muss sich dieses Release nicht vor dem gigantischen 0.46.0er Release im letzten Jahr verstecken: Macht Euch gefasst auf Objekt-Paging, das es OpenMW endlich erlaubt, entfernte Objekte effizient darzustellen; die korrekte Unterstützung von Gras-Mods; ein verbessertes Beleuchtungssystem; eine effizientere und robustere Spielphysik; die neue, optionale Über-die-Schulter-Ansicht; und vieles, vieles mehr!

Darüber hinaus haben unsere fleißigen Entwickler wieder zahllose Bugs behoben - sowohl für das Basisspiel als auch für diverse Mods, um eine noch bessere Kompatibilität mit für die Original-Engine erstellten Modifikationen zu garantieren.

Schaut Euch am besten das [englische Release-Video](https://www.youtube.com/watch?v=_9sUhduT-K4) an - oder werft einen Blick auf das [erste deutsche Release-Video](https://www.youtube.com/watch?v=9Cwdf7fwO6k), das jemals für OpenMW produziert wurde! Beide stammen aus der Feder unseres neuen Video-Zweiergespanns, dem Release-Veteran Atahualpa und dem 0.46.0-Frischling johnnyhostile, und fassen die wichtigsten Neuerungen zusammen.

Im Folgenden findet Ihr die ins Deutsche übertragene, vollständige Änderungsliste für OpenMW, Version 0.47.0. Viel Spaß beim Lesen!

**Bekannte Probleme:**
* Auf macOS kann OpenMW nur dann über den Editor (OpenMW-CS) gestartet werden, wenn 'OpenMW.app' und 'OpenMW-CS.app' als 'siblings' verbunden sind
* Lichteffekte auf Zauberpartikeln sehen zu blass aus
* Wegfindung während der "Treulosigkeiten"-Quest in der "Tribunal"-Erweiterung funktioniert nicht richtig, so dass Taren Andoren seinen Zielort möglicherweise nicht erreicht; Warten für eine Spielstunde erlaubt es Taren, sein Ziel zu erreichen, so dass das Tagebuch korrekt aktualisiert wird
* Performance verzauberter Fernkampfprojektile (z.B. Wurfpfeile) ist schlechter als die anderer Projektile

**Neue Engine-Features:**
* *von akortunov*
  * [[#5524](https://gitlab.com/OpenMW/openmw/-/issues/5524)] Fehlgeschlagene Skripte werden als 'inaktiv' markiert und ignoriert, anstatt sie komplett zu entfernen, - so lange, bis das Spiel erneut geladen oder gestartet wird
  * [[#5580](https://gitlab.com/OpenMW/openmw/-/issues/5580)] Verweigerung spezifischer NPC-Dienstleistungen (z.B. Handel oder Reparatur) über Filter ermöglicht komplexere Reaktionen anstelle des ursprünglichen "Alles oder nichts"-Verhaltens in der Original-Engine
  * [[#5642](https://gitlab.com/OpenMW/openmw/-/issues/5642)] Möglichkeit, Pfeile dem Skelett von Akteuren anstatt dem Bogen zuzuweisen, erlaubt z.B. das Erstellen von Bögen für Linkshänder oder personenabhängiger Schussanimationen
  * [[#5813](https://gitlab.com/OpenMW/openmw/-/issues/5813)] Gesonderte Behandlung von Gras-Mods mittels so genannter Gras-Instanziierung ("grass instancing") zur effizienten Darstellung von Bodenvegetation im Spiel
* *von AnyOldName3*
  * [[#4899](https://gitlab.com/OpenMW/openmw/-/issues/4899)] "Alpha-to-Coverage"-Antialiasing: verbessert das Aussehen alpha-getester Texturen, z.B. Laub-Texturen aus dem "Morrowind Optimization Patch"
  * [[#4977](https://gitlab.com/OpenMW/openmw/-/issues/4977)] OpenMW zeigt ein Platzhalter-Symbol, falls das eigentliche Symbol eines Gegenstandes nicht gefunden wurde
* *von Assumeru*
  * [[#2404](https://gitlab.com/OpenMW/openmw/-/issues/2404)] Stufenabhängige Listen ("levelled lists") können in Behältern platziert werden
  * [[#2798](https://gitlab.com/OpenMW/openmw/-/issues/2798)] Basisdatensätze sind variabel ("mutable"), d.h. das Modifizieren einer Instanz eines Basisdatensatzes (z.B. einer Wache) wirkt sich nun auf alle anderen Instanzen desselben Typs aus (z.B. alle Klone dieser Wache)
  * [[#5730](https://gitlab.com/OpenMW/openmw/-/issues/5730)] Option zur Unterstützung des sichtbaren "Aufsammelns" von Pflanzen ("graphic herbalism") im 'Advanced'-Reiter des Launchers
  * [[#5771](https://gitlab.com/OpenMW/openmw/-/issues/5771)] Der 'ori'-Konsolenbefehl zeigt nun auch die Herkunft eines Meshes an und gibt an, ob die mit dem Präfix "x" versehene Version verwendet wird
* *von bzzt*
  * Korrekte Unterwasser-Schatten bei eingeschalteter Lichtbrechung (Refraktion); deaktiviert, falls Skalierungsfaktor ('refraction scale') ungleich 1,0 *[Ergänzungen von AnyOldName3 und madsbuvi]*
  * [[#2386](https://gitlab.com/OpenMW/openmw/-/issues/2386)] Optional: Darstellung entfernter statischer Objekte mittels Objekt-Paging, d.h. Zusammenfassen benachbarter Objekte; standardmäßig eingeschaltet, benötigt aber aktivierte 'Distant land'-Option *[Korrekturen und Feinschliff von psi29a]*
  * [[#2386](https://gitlab.com/OpenMW/openmw/-/issues/2386)] Optional: Objekt-Paging in aktiven Zellen (3x3-Gitter um den Helden herum); standardmäßig eingeschaltet *[Korrekturen und Feinschliff von psi29a]*
* *von Capostrophic*
  * [[#5362](https://gitlab.com/OpenMW/openmw/-/issues/5362)] Dialogfenster für das Aufteilen von Gegenständen zeigt den Namen der in einem Stapel Seelensteine gefangenen Seele an
  * [[#5445](https://gitlab.com/OpenMW/openmw/-/issues/5445)] Verarbeitung der 'NiLines'-Eigenschaft in NIF-Modellen
  * [[#5545](https://gitlab.com/OpenMW/openmw/-/issues/5545)] Optional: Zu Boden gegangene (bewusstlose) NPCs können bestohlen werden; standardmäßig ausgeschaltet
  * [[#5579](https://gitlab.com/OpenMW/openmw/-/issues/5579)] Unterstützung der Rotation in umgekehrter Reihenfolge (z-, y-, dann x-Achse) in der 'SetAngle'-Funktion
  * [[#5649](https://gitlab.com/OpenMW/openmw/-/issues/5649)] Unterstützung des komprimierten BSA-Formats der "TES V: Skyrim Special Edition"
  * [[#5672](https://gitlab.com/OpenMW/openmw/-/issues/5672)] Option für an die Bildschirmauflösung angepasste Menühintergründe ist nun auch im Launcher verfügbar
* *von CedricMocquillon*
  * [[#1536](https://gitlab.com/OpenMW/openmw/-/issues/1536)] Anzeige der momentan erreichten Stufenaufstieg-Multiplikatoren für Attribute im 'Stufe'-Tooltip
  * [[#4486](https://gitlab.com/OpenMW/openmw/-/issues/4486)] Behandlung und Aufzeichnung von Abstürzen auf Windows
  * [[#5297](https://gitlab.com/OpenMW/openmw/-/issues/5297)] Suchleiste im 'Data Files'-Reiter des Launchers
  * [[#5486](https://gitlab.com/OpenMW/openmw/-/issues/5486)] Optional: Angebotene Fertigkeiten von Ausbildern und deren Höchstgrenze werden durch Basis- statt modifizierte Werte bestimmt; standardmäßig ausgeschaltet
  * [[#5519](https://gitlab.com/OpenMW/openmw/-/issues/5519)] Neugeordneter und erweiterter 'Advanced'-Reiter im Launcher, um analog zum MCP ("Morrowind Code Patch") implementierte Einstellungen zu integrieren
  * [[#5814](https://gitlab.com/OpenMW/openmw/-/issues/5814)] 'bsatool' kann bestehende BSA-Archive erweitern oder neue erstellen (unterstützt alle BSA-Formate)
* *von elsid*
  * [[#4917](https://gitlab.com/OpenMW/openmw/-/issues/4917)] Objekte, die zu klein sind, um das Navigationsnetz ("navigation mesh") zu beeinflussen, veranlassen nun keine Aktualisierung dieses Netzes mehr, was dessen Effizienz erhöht
  * [[#5500](https://gitlab.com/OpenMW/openmw/-/issues/5500)] Das Laden einer Spielszene endet nun erst dann, wenn genug Teile des Navigationsnetzes um den Helden herum erstellt worden sind
  * [[#6033](https://gitlab.com/OpenMW/openmw/-/issues/6033)] Existierende Wegpunkte-Gitter werden nun automatisch dem Navigationsnetz hinzugefügt, um die Wegfindung weiter zu verbessern
  * [[#6033](https://gitlab.com/OpenMW/openmw/-/issues/6033)] Rückfall in den alten Wegfindungsalgorithmus teilweise entfernt, um z.B. zu verhindern, dass Akteure ihre Feinde durch Lavaströme verfolgen
  * [[#6034](https://gitlab.com/OpenMW/openmw/-/issues/6034)] Berechnung optimaler Routen basierend auf der individuellen Geschwindigkeit eines Akteurs zu Land und zu Wasser
* *von fr3dz10*
  * [[#2159](https://gitlab.com/OpenMW/openmw/-/issues/2159)] Optional: Gesprächsthemen, die zum Zeitpunkt des Dialogs keine neuen Antworten brächten, werden ausgegraut; neue Themen werden farblich hervorgehoben; standardmäßig ausgeschaltet (die Farben sind einstellbar)
  * [[#4201](https://gitlab.com/OpenMW/openmw/-/issues/4201)] Kollisionen zwischen Projektilen inkl. Zaubern (ein erfolgreicher Treffer neutralisiert beide Projektile); imitiert das Verhalten in der Original-Engine
  * [[#5563](https://gitlab.com/OpenMW/openmw/-/issues/5563)] Optional: Physik-Berechnungen werden in einem oder mehreren Hintergrund-Threads abgearbeitet, um die Effizienz zu erhöhen; standardmäßig ausgeschaltet
* *von glassmancody.info*
  * [[#5828](https://gitlab.com/OpenMW/openmw/-/issues/5828)] Neues, Shader-basiertes, anpassbares Beleuchtungssystem, das das bisherige Maximum von acht Lichtquellen pro Objekt entfernt
* *von jefetienne*
  * [[#5692](https://gitlab.com/OpenMW/openmw/-/issues/5692)] Der Filter im Zauberspruch-Fenster zeigt nun auch Gegenstände und Zauber mit zutreffenden magischen Effekten an - nicht nur solche mit zutreffendem Namen
* *von ptmikheev*
  * Verbesserte freie Heldenansicht in der Verfolgerperspektive
  * [[#390](https://gitlab.com/OpenMW/openmw/-/issues/390)] Optional: Kamera schaut in Verfolgerperspektive über die Schulter des Helden; standardmäßig ausgeschaltet
  * [[#390](https://gitlab.com/OpenMW/openmw/-/issues/390)] Optional: Automatisches Wechseln der Kamera zur anderen Schulter in engen Gängen; standardmäßig eingeschaltet, benötigt aber aktivierte 'View over the shoulder'-Option
  * [[#2686](https://gitlab.com/OpenMW/openmw/-/issues/2686)] Aufgezeichnete Informationen in der 'openmw.log'-Datei werden mit Zeitstempeln versehen
  * [[#4894](https://gitlab.com/OpenMW/openmw/-/issues/4894)] Optional: NPCs vermeiden Kollisionen beim Laufen; standardmäßig ausgeschaltet
  * [[#4894](https://gitlab.com/OpenMW/openmw/-/issues/4894)] Optional: NPCs machen Platz für sich bewegende Akteure, falls sie gerade nichts Wichtiges zu tun haben; standardmäßig eingeschaltet, benötigt aber aktivierte 'NPCs avoid collisions'-Option
  * [[#5043](https://gitlab.com/OpenMW/openmw/-/issues/5043)] Optional: Kamera schwankt beim Laufen in der Egoperspektive ("head bobbing"); standardmäßig ausgeschaltet (Stärke des Effekts einstellbar)
  * [[#5457](https://gitlab.com/OpenMW/openmw/-/issues/5457)] Optional: Verbesserte Charakteranimationen bei diagonaler Fortbewegung; standardmäßig ausgeschaltet
  * [[#5610](https://gitlab.com/OpenMW/openmw/-/issues/5610)] Optional: Akteure laufen weicher und drehen sich weniger ruckartig um; standardmäßig ausgeschaltet
* *von simonmeulenbeek*
  * [[#5511](https://gitlab.com/OpenMW/openmw/-/issues/5511)] Audio-Einstellungen im 'Advanced'-Reiter des Launchers hinzugefügt
* *von TescoShoppah*
  * [[#3983](https://gitlab.com/OpenMW/openmw/-/issues/3983)] Installationsassistent verlinkt nun den FAQ-Abschnitt über Möglichkeiten des (legalen!) Erwerbs einer Morrowind-Kopie
* *von unelsson*
  * [[#5456](https://gitlab.com/OpenMW/openmw/-/issues/5456)] Grundlegende Unterstützung von 'Collada'-Animationen
* *von wareya*
  * [[#5762](https://gitlab.com/OpenMW/openmw/-/issues/5762)] Der Bewegungslöser ("movement solver") ist nun deutlich robuster
  * [[#5910](https://gitlab.com/OpenMW/openmw/-/issues/5910)] Spielphysik greift im Notfall auf so genannte harte Delta-Zeit ("true delta time") zurück, um die Berechnungsdauer pro Frame zu begrenzen und so ein Einfrieren (und Abstürzen) des Spiels zu vermeiden

**Neue Editor-Features:**
* *von Atahualpa*
  * [[#6024](https://gitlab.com/OpenMW/openmw/-/issues/6024)] Einstellbare Aktionen für primäre und sekundäre Auswahltasten beim Editieren von Terrain ('Terrain land editing', siehe [#3171](https://gitlab.com/OpenMW/openmw/-/issues/3171))
* *von olcoal*
  * [[#5199](https://gitlab.com/OpenMW/openmw/-/issues/5199)] Bessere und einstellbare Hintergrundfarben in der 3D-Ansicht
* *von unelsson*
  * [[#832](https://gitlab.com/OpenMW/openmw/-/issues/832)] Korrekte Behandlung gelöschter Instanzen
  * [[#3171](https://gitlab.com/OpenMW/openmw/-/issues/3171)] Objektauswahl in der 3D-Ansicht: zentrierter Würfel ('Centred cube'), an Ecke aufgezogener Würfel ('Cube corner to corner'), zentrierte Kugel ('Centred sphere')
  * [[#3171](https://gitlab.com/OpenMW/openmw/-/issues/3171)] Konfigurierbare Aktionen für die Objektauswahl: auswählen ('Select only'), zur Auswahl hinzufügen ('Add to selection'), von Auswahl abziehen ('Remove from selection'), Auswahl umkehren ('Invert selection')

**Behobene Engine-Bugs:**
* *von akortunov*
  * [[#3714](https://gitlab.com/OpenMW/openmw/-/issues/3714)] Konflikte zwischen 'SpellState'- und 'MagicEffects'-Funktionalität der Engine behoben
  * [[#3789](https://gitlab.com/OpenMW/openmw/-/issues/3789)] Das Aktualisieren der aktiven magischen Effekte sollte nun nicht mehr zu Abstürzen führen
  * [[#4021](https://gitlab.com/OpenMW/openmw/-/issues/4021)] Werte von Attributen und Fertigkeiten werden nun als Fließkommazahlen gespeichert
  * [[#4623](https://gitlab.com/OpenMW/openmw/-/issues/4623)] Implementierung der Corprus-Krankheit wurde verbessert
  * [[#5108](https://gitlab.com/OpenMW/openmw/-/issues/5108)] Die Speichergröße von Spielständen wird nicht länger durch ein ungeeignetes Nebel-Texturformat aufgebläht
  * [[#5165](https://gitlab.com/OpenMW/openmw/-/issues/5165)] Aktive Zaubereffekte werden nun in Echtzeit aktualisiert, anstatt Zeitstempel zu verwenden, was Probleme mit der Zeitskalierung im Spiel und mit geskripteten Tageszeitänderungen behebt
  * [[#5387](https://gitlab.com/OpenMW/openmw/-/issues/5387)] 'Move'- und 'MoveWorld'-Befehle aktualisieren nun die Zelle des verschobenen Objektes in korrekter Weise
  * [[#5499](https://gitlab.com/OpenMW/openmw/-/issues/5499)] Die Spiellogik für Beförderungen innerhalb einer Fraktion berücksichtigt nun auch die zweite der beiden bevorzugten Fertigkeiten der jeweiligen Fraktion
  * [[#5502](https://gitlab.com/OpenMW/openmw/-/issues/5502)] Die Totzone für Analog-Sticks kann nun in der 'openmw.cfg'-Datei eingestellt werden
  * [[#5619](https://gitlab.com/OpenMW/openmw/-/issues/5619)] Tastatureingaben während des Ladens von Spielständen werden nun ignoriert
  * [[#5975](https://gitlab.com/OpenMW/openmw/-/issues/5975)] Kontrollpunkte so genannter "Sheath"-Modelle (die in Mods mit wegsteckbaren Waffen genutzt werden) werden nun deaktiviert, um z.B. das versehentliche Abspielen von Schussanimationen für Bögen der "HQ Arsenal"-Mod zu verhindern
  * [[#6043](https://gitlab.com/OpenMW/openmw/-/issues/6043)] Schild-Animationen von NPCs werden nun abgebrochen, sobald eine Lichtquelle an Stelle des Schildes ausgerüstet wird
  * [[#6047](https://gitlab.com/OpenMW/openmw/-/issues/6047)] Tastenbelegungen für Maustasten können nun nicht mehr ausgelöst werden, wenn die Steuerung im Spiel deaktiviert ist, z.B. während des Ladens eines Spielstandes
* *von AnyOldName3*
  * [[#2069](https://gitlab.com/OpenMW/openmw/-/issues/2069)] 'NiFlipController' in NIF-Modellen werden nun nur noch auf die Basistextur angewendet, was z.B. Darstellungsprobleme mit den Glühwürmchen aus der Mod "Fireflies Invade Morrowind" behebt
  * [[#2976](https://gitlab.com/OpenMW/openmw/-/issues/2976)] Probleme mit der Priorität der lokalen und globalen Konfigurationsdateien behoben
  * [[#4631](https://gitlab.com/OpenMW/openmw/-/issues/4631)] Grafikkarten, die einen Antialiasing-Faktor von 16 nicht unterstützen, verwenden nun automatisch einen geringeren Wert, wenn in den Optionen '16' eingestellt wird
  * [[#5391](https://gitlab.com/OpenMW/openmw/-/issues/5391)] Charaktermodelle aus der Mod "Races Redone" werden nun im Inventar korrekt angezeigt
  * [[#5688](https://gitlab.com/OpenMW/openmw/-/issues/5688)] Der Wasser-Shader wird nun in Innenräumen auch dann korrekt ausgeführt, wenn Schatteneffekte in Innenräumen deaktiviert worden sind
  * [[#5906](https://gitlab.com/OpenMW/openmw/-/issues/5906)] Sonnenblendeffekt ("sun glare") funktioniert nun auch mit 'Mesa'-Treibern und AMD-Grafikkarten
* *von Assumeru*
  * [[#2311](https://gitlab.com/OpenMW/openmw/-/issues/2311)] NPCs, die nicht einzigartig sind (z.B. Wachen), können nun Ziel von Skripten sein
  * [[#2473](https://gitlab.com/OpenMW/openmw/-/issues/2473)] Wird einem Händler eine Ware verkauft, deren Bestand immer sofort aufgefüllt wird, so erhöht sich nun dauerhaft die angebotene Menge dieser Ware
  * [[#3862](https://gitlab.com/OpenMW/openmw/-/issues/3862)] Zufällige Inhalte von Behältern werden nun ähnlich wie in der Original-Engine bestimmt
  * [[#3929](https://gitlab.com/OpenMW/openmw/-/issues/3929)] Behälter mit zufällig generiertem Inhalt werden nun nicht wieder aufgefüllt, wenn der Held sie plündert und anschließend mit dem Besitzer des Behälters handelt
  * [[#4039](https://gitlab.com/OpenMW/openmw/-/issues/4039)] Gefolgsleute folgen ihrem Anführer nun nicht mehr im Gänsemarsch, sondern halten alle den gleichen Abstand zu ihm ein
  * [[#4055](https://gitlab.com/OpenMW/openmw/-/issues/4055)] Falls eine lokale Instanz eines Skriptes erzeugt wird und bereits eine globale Instanz desselben Skripts aktiv ist, so werden die Variablen des lokalen Skripts nun mit den Werten des globalen initialisiert
  * [[#5300](https://gitlab.com/OpenMW/openmw/-/issues/5300)] NPCs wechseln von Fackel zu Schild, wenn sie in einen Kampf geraten *[inspiriert von Capostrophics Vorarbeit]*
  * [[#5423](https://gitlab.com/OpenMW/openmw/-/issues/5423)] Größere Kreaturen, z.B. Guars, die einem Akteur folgen, rempeln diesen nun nicht mehr an
  * [[#5469](https://gitlab.com/OpenMW/openmw/-/issues/5469)] Geskriptete Drehungen oder Verschiebungen von großen Objekten setzen nicht länger den Nebel des Krieges der Umgebungskarte zurück
  * [[#5661](https://gitlab.com/OpenMW/openmw/-/issues/5661)] Das Soundsystem nutzt nun Rückfallwerte, um die minimale und maximale Zeit zwischen regionsabhängigen Umgebungsgeräuschen zu bestimmen
  * [[#5661](https://gitlab.com/OpenMW/openmw/-/issues/5661)] Es besteht nun die Möglichkeit, dass kein Umgebungsgeräusch abgespielt wird, falls die Summe der Wahrscheinlichkeiten aller Geräusche kleiner als 100 % ist
  * [[#5687](https://gitlab.com/OpenMW/openmw/-/issues/5687)] Gleichzeitig ausklingende Beschwörungszauber für Gegenstände, die denselben Inventarplatz belegen, lassen das Spiel nicht länger einfrieren
  * [[#5835](https://gitlab.com/OpenMW/openmw/-/issues/5835)] Skripte können nun KI-Werte ('AI Hello', 'AI Alarm', 'AI Fight', 'AI Flee') auf negative Werte setzen
  * [[#5836](https://gitlab.com/OpenMW/openmw/-/issues/5836)] Voraussetzungen für Gesprächsantworten, die negative KI-Werte als Bedingung enthalten, funktionieren nun korrekt
  * [[#5838](https://gitlab.com/OpenMW/openmw/-/issues/5838)] Teleport-Türen, deren Zielort nichtexistierende, namenlose Zellen sind, korrumpieren nun nicht mehr die Umgebungskarte
  * [[#5840](https://gitlab.com/OpenMW/openmw/-/issues/5840)] Angreifende NPCs, die von einem Feuer-/Frost-/Schockschildeffekt des Feindes getroffen werden, lösen nun den entsprechenden Sound-Effekt aus
  * [[#5841](https://gitlab.com/OpenMW/openmw/-/issues/5841)] Kostenlose Zauber können nun auch dann gewirkt werden, wenn der zaubernde Akteur keine Magiepunkte mehr hat
  * [[#5871](https://gitlab.com/OpenMW/openmw/-/issues/5871)] Benutzer mit russischem Tastatur-Layout können nun das 'Ё'-Zeichen in Eingabefeldern verwenden, ohne dass sich die Konsole öffnet
  * [[#5912](https://gitlab.com/OpenMW/openmw/-/issues/5912)] Effekte von Beschwörungszaubern werden bei Fehlschlagen der Beschwörung nicht mehr sofort entfernt
  * [[#5923](https://gitlab.com/OpenMW/openmw/-/issues/5923)] Mausklicks auf textfreie Flächen im Tagebuch können nun nicht mehr zum spontanen Öffnen von Themen auf der nächsten Seite führen
  * [[#5934](https://gitlab.com/OpenMW/openmw/-/issues/5934)] Kompatibilität zur Original-Engine: Die angegebene Anzahl der hinzuzufügenden Gegenstände in 'AddItem'-Skriptbefehlen wird nun in einen vorzeichenlosen Wert umgerechnet, so dass negative Werte "überlaufen" und zum Hinzufügen einer positiven Menge von Gegenständen führen
  * [[#5991](https://gitlab.com/OpenMW/openmw/-/issues/5991)] Skripte können nun Bücher und Schriftrollen aktivieren, wenn sich das Spiel in der Inventaransicht befindet
  * [[#6007](https://gitlab.com/OpenMW/openmw/-/issues/6007)] Fehlerhafte Videodateien können nicht länger zu Spielabstürzen führen
  * [[#6016](https://gitlab.com/OpenMW/openmw/-/issues/6016)] Schleichende oder springende NPCs halten nun nicht mehr an und drehen sich auch nicht zur Begrüßung des Helden um
* *von Capostrophic*
  * [[#4774](https://gitlab.com/OpenMW/openmw/-/issues/4774)] Wachen ignorieren nicht länger Angriffe eines unsichtbaren Helden, sondern sprechen diesen an und fliehen, falls sich der Held der Verhaftung widersetzt
  * [[#5358](https://gitlab.com/OpenMW/openmw/-/issues/5358)] Das Ansprechen eines anderen Akteurs, ohne das Dialogfenster vorher zu schließen, löscht nun nicht mehr den Gesprächsverlauf, was z.B. Gespräche mit mehreren Teilnehmern mittels 'ForceGreeting'-Befehl ermöglicht
  * [[#5363](https://gitlab.com/OpenMW/openmw/-/issues/5363)] Das 'Auto Calc'-Flag für Verzauberungen wird nun von OpenMW-CS und OpenMWs 'esmtool' auch als solches behandelt
  * [[#5364](https://gitlab.com/OpenMW/openmw/-/issues/5364)] Skripte, die ein nichtexistierendes globales Skript aufrufen wollen, überspringen diesen Schritt nun einfach, statt ihre Ausführung komplett abzubrechen
  * [[#5367](https://gitlab.com/OpenMW/openmw/-/issues/5367)] Bei Auswahl eines bereits ausgerüsteten Zaubers oder magischen Gegenstandes über eine Schnelltaste wird nun nicht mehr das Ausrüsten-Geräusch abgespielt
  * [[#5369](https://gitlab.com/OpenMW/openmw/-/issues/5369)] Der Skalierungswert in stufenabhängigen Kreaturenlisten wird nun auf die Größe der erzeugten Kreaturen angewendet
  * [[#5370](https://gitlab.com/OpenMW/openmw/-/issues/5370)] Ach, Bethesda: Das Benutzen eines Schlüssels zum Öffnen einer fallenbewehrten Tür oder eines fallenbewehrten Behälters entschärft nur noch dann die Falle, wenn die Tür bzw. der Behälter auch tatsächlich abgeschlossen ist
  * [[#5397](https://gitlab.com/OpenMW/openmw/-/issues/5397)] NPC-Begrüßungszeilen werden nun korrekt zurückgesetzt, wenn ein Bereich verlassen und erneut betreten wird
  * [[#5403](https://gitlab.com/OpenMW/openmw/-/issues/5403)] Visuelle Zaubereffekte, die auf einen Akteur wirken, werden nun während dessen Sterbeanimation weiter abgespielt
  * [[#5415](https://gitlab.com/OpenMW/openmw/-/issues/5415)] Modelle ausgerüsteter Gegenstände mit Environment-Maps, z.B. die Rüstungen der "HiRez Armors"-Mod, zeigen diesen Effekt nun auch
  * [[#5416](https://gitlab.com/OpenMW/openmw/-/issues/5416)] In NIF-Modellen werden unbrauchbare Wurzeln, die keine Knoten sind, ("non-node root records") nun weniger streng behandelt, damit Ressourcen bestimmter Mods geladen werden können
  * [[#5424](https://gitlab.com/OpenMW/openmw/-/issues/5424)] Kreaturen wenden ihre Köpfe nun dem Helden zu
  * [[#5425](https://gitlab.com/OpenMW/openmw/-/issues/5425)] Magische Effekte, deren Wirkung nicht einmalig ist, haben nun eine Mindestlaufzeit von einer Sekunde
  * [[#5427](https://gitlab.com/OpenMW/openmw/-/issues/5427)] Die 'GetDistance'-Funktion stoppt nun nicht mehr die Ausführung von Skripten, falls kein Objekt mit der übergebenen ID existiert
  * [[#5427](https://gitlab.com/OpenMW/openmw/-/issues/5427)] Die 'GetDistance'-Funktion und die interne Objektsuche zeichnen nun bessere Warnmeldungen auf, falls Objekte für eine gegebene ID fehlen
  * [[#5435](https://gitlab.com/OpenMW/openmw/-/issues/5435)] Feindliche Angriffe können den Helden nun auch dann treffen, wenn die Kollisionsberechnung ausgeschaltet ist (Konsole: 'tcl')
  * [[#5441](https://gitlab.com/OpenMW/openmw/-/issues/5441)] Die Priorität bestimmter Animationen wurde korrigiert, so dass Feinde den Helden nun wegstoßen können, wenn Letzterer in der Egoperspektive zum Schlag ausholt und diese Position einfach hält
  * [[#5451](https://gitlab.com/OpenMW/openmw/-/issues/5451)] Magische Projektile verschwinden nun sofort, wenn ihre Quelle verschwindet oder stirbt
  * [[#5452](https://gitlab.com/OpenMW/openmw/-/issues/5452)] Automatisches Laufen wird nicht mehr in Spielständen gespeichert, so dass der Held in diesem Fall nach dem Laden wieder stillsteht
  * [[#5484](https://gitlab.com/OpenMW/openmw/-/issues/5484)] Gegenstände mit einem Basiswert von 0 können nun nicht mehr für ein Goldstück verkauft werden
  * [[#5485](https://gitlab.com/OpenMW/openmw/-/issues/5485)] Erfolgreiche Einschüchterungsversuche erhöhen nun die Stimmung des Gesprächspartners wenigstens um einen Minimalwert
  * [[#5490](https://gitlab.com/OpenMW/openmw/-/issues/5490)] Treffer auf den linken Waffenslot werden nun auf den linken Schulter- bzw. den Brustplatten-Slot umgeleitet, falls der Akteur keinen Schild ausgerüstet hat
  * [[#5525](https://gitlab.com/OpenMW/openmw/-/issues/5525)] Das Suchfenster im Inventar ignoriert Groß- und Kleinschreibung nun auch für Nicht-ASCII-Zeichen
  * [[#5603](https://gitlab.com/OpenMW/openmw/-/issues/5603)] Beim Wechsel zu konstanten Zaubereffekten im Verzauberungsmenü werden nun alle Effekte auf 'Sich selbst' gesetzt, oder aber entfernt, falls sie nicht auf den Zaubernden selbst gewirkt werden können
  * [[#5604](https://gitlab.com/OpenMW/openmw/-/issues/5604)] OpenMWs Ladekomponenten für Meshes können nun NIF-Dateien mit mehreren Wurzeln ("root nodes") richtig interpretieren
  * [[#5611](https://gitlab.com/OpenMW/openmw/-/issues/5611)] Reparaturhämmer mit 0 Anwendungen können nun nur noch ein Mal benutzt werden, bevor sie kaputtgehen; Dietriche und Sonden mit 0 Anwendungen bewirken hingegen gar nichts - danke, Bethesda!
  * [[#5622](https://gitlab.com/OpenMW/openmw/-/issues/5622)] Die Priorität des Hauptmenü-Fensters wurde verringert, um zu verhindern, dass die Konsole den Mausfokus verliert und nicht mehr erreichbar ist
  * [[#5627](https://gitlab.com/OpenMW/openmw/-/issues/5627)] Der Syntaxanalysierer (Parser) für Schriftstücke berücksichtigt nun Bilder und Formatierungs-Tags, die auf das letzte Zeilenende-Tag ("<br>") folgen, damit Bücher bestimmter Mods korrekt dargestellt werden können
  * [[#5633](https://gitlab.com/OpenMW/openmw/-/issues/5633)] Negative Zaubereffekte, die vor dem Wechsel in den Gottmodus (Konsole: 'tgm') gewirkt wurden, schädigen den Helden nicht weiter
  * [[#5639](https://gitlab.com/OpenMW/openmw/-/issues/5639)] Tooltips verdecken nun nicht mehr Hinweisfenster im Spiel
  * [[#5644](https://gitlab.com/OpenMW/openmw/-/issues/5644)] Aktive Beschwörungseffekte auf dem Helden führen nicht länger zu Abstürzen während des Spielstarts
  * [[#5656](https://gitlab.com/OpenMW/openmw/-/issues/5656)] Charaktere, die sich im Schleichen-Modus befinden, wechseln beim Blocken von Angriffen nun nicht mehr temporär in eine aufrechte Haltung
  * [[#5695](https://gitlab.com/OpenMW/openmw/-/issues/5695)] Akteure, die über einen Skriptbefehl zum Wirken eines Fernzaubers auf sich selbst gezwungen werden, zielen nun auf ihre Füße statt in die Richtung ihres aktuellen Ziels
  * [[#5706](https://gitlab.com/OpenMW/openmw/-/issues/5706)] KI-Sequenzen (z.B. für patrouillierende NPCs) werden nun beim erneuten Laden eines Spielstandes weiterhin in Endlosschleife ausgeführt
  * [[#5758](https://gitlab.com/OpenMW/openmw/-/issues/5758)] Unter Wasser befindliche Akteure treiben nun an die Wasseroberfläche, wenn sie gelähmt sind
  * [[#5758](https://gitlab.com/OpenMW/openmw/-/issues/5758)] Akteure unter Einfluss eines Levitationszaubers fallen nun zu Boden, wenn sie gelähmt werden
  * [[#5869](https://gitlab.com/OpenMW/openmw/-/issues/5869)] Wachen konfrontieren den Helden nun nur noch dann mit einem Verbrechen, falls sich dieser in Sichtweite befindet
  * [[#5877](https://gitlab.com/OpenMW/openmw/-/issues/5877)] Die Transparenz von Symbolen für aktive magische Effekte wird nun korrekt zurückgesetzt, um das Auftauchen "leerer" Symbole in bestimmten Situationen zu verhindern
  * [[#5902](https://gitlab.com/OpenMW/openmw/-/issues/5902)] 'NiZBufferProperty' verarbeitet nun 'depth test'-Flag
  * [[#5995](https://gitlab.com/OpenMW/openmw/-/issues/5995)] Der so genannte UV-Versatz ("UV offset") in 'NiUVControllern' - der in der Original-Engine zur Simulation der Bewegung von Flüssigkeiten genutzt wird - wird nun korrekt berechnet
* *von ccalhoun1999*
  * [[#5101](https://gitlab.com/OpenMW/openmw/-/issues/5101)] Feindlich gesonnene Begleiter folgen dem Helden nun nicht mehr durch Türen und nutzen auch keine Reisedienstleistungen mit ihm zusammen
* *von davidcernat*
  * [[#5422](https://gitlab.com/OpenMW/openmw/-/issues/5422)] Euer Held verliert nun nicht mehr alle seine Zaubersprüche, wenn er mit dem 'resurrect'-Konsolenbefehl wiederbelebt wird
* *von elsid*
  * [[#4764](https://gitlab.com/OpenMW/openmw/-/issues/4764)] Haupt- und Rendering-Thread werden nun synchronisiert, um Fehler beim Erzeugen von Partikeln zu verhindern, z.B. bei Wasserwellen
  * [[#5479](https://gitlab.com/OpenMW/openmw/-/issues/5479)] Fehler bei der KI-Wegfindung behoben, der dazu führte, dass NPCs wie angewurzelt herumstanden, statt durch die Gegend zu laufen
  * [[#5507](https://gitlab.com/OpenMW/openmw/-/issues/5507)] Alle Lautstärkeeinstellungen sind nun immer auf den Bereich [0,0; 1,0] beschränkt und können diesen nicht mehr potenziell durch Änderungen in den Spieleinstellungen verlassen
  * [[#5531](https://gitlab.com/OpenMW/openmw/-/issues/5531)] Fliehende Akteure werden nun korrekt gedreht, um z.B. zu verhindern, dass Klippenläufer auf der Flucht ins Wasser tauchen
  * [[#5914](https://gitlab.com/OpenMW/openmw/-/issues/5914)] Der interne 'Navigator' erstellt nun lokal beschränkte Pfade für Akteure mit weit entfernten Zielen, um sicherzustellen, dass die Wegfindung auch über große Distanzen (außerhalb des Navigationsnetzes) funktioniert
  * [[#6294](https://gitlab.com/OpenMW/openmw/-/issues/6294)] Spielabsturz behoben, der von leeren Wegpunkte-Gittern verursacht wurde
* *von fr3dz10*
  * [[#3372](https://gitlab.com/OpenMW/openmw/-/issues/3372)] Magische und nicht-magische Projektile kollidieren nun immer mit sich bewegenden Zielen
  * [[#4083](https://gitlab.com/OpenMW/openmw/-/issues/4083)] Sich öffnende oder schließende Türen imitieren beim Zusammenstoßen mit einem Akteur nun das Verhalten der Original-Engine *[teilweise behoben von elsid]*
  * [[#5472](https://gitlab.com/OpenMW/openmw/-/issues/5472)] "Zero-Lifetime"-Partikel werden nun korrekt verarbeitet und eine zugehörige potenzielle Null-Division im 'NiParticleColorModifier', die zu Abstürzen auf Nicht-PC-Betriebssystemen führte, wenn z.B. Melchior Dahrks "Mistify"-Mod verwendet wurde, wurde behoben
  * [[#5548](https://gitlab.com/OpenMW/openmw/-/issues/5548)] Die 'ClearInfoActor'-Skriptfunktion löscht nun immer das korrekte Gesprächsthema eines Akteurs
  * [[#5739](https://gitlab.com/OpenMW/openmw/-/issues/5739)] Fallschaden kann nun nicht mehr durch Speichern und Neuladen kurz vor dem Aufprall vermieden werden
* *von glassmancody.info*
  * [[#5899](https://gitlab.com/OpenMW/openmw/-/issues/5899)] Noch geöffnete Fenster mit Entscheidungsmöglichkeiten führen bei Beenden des Spiels nun nicht mehr zu Abstürzen
  * [[#6028](https://gitlab.com/OpenMW/openmw/-/issues/6028)] NIF-Partikelsysteme erben nun die Partikel-Anzahl aus ihren 'NiParticleData'-Einträgen auf korrekte Weise, was z.B. Probleme mit der Mod "I Lava Good Mesh Replacer" behebt
* *von kyleshrader*
  * [[#5588](https://gitlab.com/OpenMW/openmw/-/issues/5588)] Mausklicks auf leere Seiten im Tagebuch führen nicht länger zum Aufschlagen zufälliger Gesprächsthemen
* *von madsbuvi*
  * [[#5539](https://gitlab.com/OpenMW/openmw/-/issues/5539)] Wechsel von niedrigerer zu Vollbild-Auflösung führen nicht länger zu einem fehlerhaft skalierten Spielfenster
* *von mp3butcher*
  * [[#1952](https://gitlab.com/OpenMW/openmw/-/issues/1952), [#3676](https://gitlab.com/OpenMW/openmw/-/issues/3676)] 'NiParticleColorModifier' in NIF-Dateien werden nun richtig behandelt, was u.a. Probleme mit Feuer- und Rauch-Partikeleffekten behebt
* *von ptmikheev*
  * [[#5557](https://gitlab.com/OpenMW/openmw/-/issues/5557)] Seitwärtsbewegungen mit einem Analog-Stick resultieren nun nicht mehr in einer langsameren Bewegungsgeschwindigkeit verglichen mit der normalen Tastatureingabe
  * [[#5821](https://gitlab.com/OpenMW/openmw/-/issues/5821)] NPCs, die von einer Mod hinzugefügt und in eine andere Zelle bewegt worden sind, werden nun korrekt von OpenMW registriert, wenn sich die Position der Mod in der Ladereihenfolge ändert
* *von SaintMercury*
  * [[#5680](https://gitlab.com/OpenMW/openmw/-/issues/5680)] Akteure zielen beim Wirken magischer Projektile nun auf die richtige Höhe, was z.B. verhindert, dass Netchbullen andauernd über den Kopf des Helden hinweg zielen
* *von Tankinfrank*
  * [[#5800](https://gitlab.com/OpenMW/openmw/-/issues/5800)] Das Ausrüsten eines Ringes mit konstanter Verzauberung kann nun nicht länger zum Ablegen eines Ringes mit auslösbarem Zauber führen, falls dieser Zauber aktuell im Zaubermenü ausgewählt ist
* *von wareya*
  * [[#1901](https://gitlab.com/OpenMW/openmw/-/issues/1901)] Das Kollisionsverhalten von Akteuren ähnelt nun noch mehr dem der Original-Engine
  * [[#3137](https://gitlab.com/OpenMW/openmw/-/issues/3137)] Laufen auf der Stelle in eine Wand hinein hindert den Helden nun nicht mehr daran, zu springen
  * [[#4247](https://gitlab.com/OpenMW/openmw/-/issues/4247)] Dank der Nutzung von AABB-Kollisionskörpern ("axis-aligned bounding box") können Akteure nun bestimmte, ehemals problematische steile Treppen erklimmen
  * [[#4447](https://gitlab.com/OpenMW/openmw/-/issues/4447)] Die Kollisionskörper von Akteuren wurden angepasst, um den Helden daran zu hindern, durch bestimmte Wände zu spähen
  * [[#4465](https://gitlab.com/OpenMW/openmw/-/issues/4465)] NPCs zucken nun nicht mehr unkontrolliert, falls ihr Kollisionskörper mit einem anderen überlappt
  * [[#4476](https://gitlab.com/OpenMW/openmw/-/issues/4476)] Der Held schwebt während Echtzeit-Reisen in abots "Gondoliers"-Mod nicht länger in der Luft
  * [[#4568](https://gitlab.com/OpenMW/openmw/-/issues/4568)] Akteure können sich nicht mehr gegenseitig durch Levelgrenzen schieben, wenn zu viele von ihnen auf einem Haufen sind
  * [[#5431](https://gitlab.com/OpenMW/openmw/-/issues/5431)] Szenen mit sehr, sehr vielen Akteuren können nun nicht mehr zu unaufhaltsamen Abwärtsspiralen in der Physik-Performance führen
  * [[#5681](https://gitlab.com/OpenMW/openmw/-/issues/5681)] Helden kollidieren nun korrekt mit Holzbrücken, anstatt steckenzubleiben oder durch sie hindurchzurennen

**Behobene Editor-Bugs:**
* *von akortunov*
  * [[#1662](https://gitlab.com/OpenMW/openmw/-/issues/1662)] OpenMW-CS stürzt nicht länger ab, falls ein Datei- oder ein Konfigurationspfad Nicht-ASCII-Symbole enthalten
* *von Atahualpa*
  * [[#5473](https://gitlab.com/OpenMW/openmw/-/issues/5473)] Zellgrenzen werden nun korrekt eingezeichnet, wenn Änderungen am Terrain rückgängig gemacht oder wiederholt werden
  * [[#6022](https://gitlab.com/OpenMW/openmw/-/issues/6022)] Das Terrain-Auswahlgitter wird nun korrekt eingezeichnet, wenn Änderungen am Terrain rückgängig gemacht oder wiederholt werden
  * [[#6023](https://gitlab.com/OpenMW/openmw/-/issues/6023)] Objekte in der 3D-Ansicht blockieren nun nicht mehr das Auswählen von Terrain im 'Terrain land editing'-Modus
  * [[#6035](https://gitlab.com/OpenMW/openmw/-/issues/6035)] Kreis-Auswahlwerkzeug wählt nicht länger Terrain außerhalb des Kreisbogens aus
  * [[#6036](https://gitlab.com/OpenMW/openmw/-/issues/6036)] Terrain-Auswahlwerkzeuge ignorieren nicht länger Vertices an der Nordost- und an der Südwest-Ecke einer Zelle
* *von Capostrophic*
  * [[#5400](https://gitlab.com/OpenMW/openmw/-/issues/5400)] Der Datenprüfer ('Verifier') prüft Körperteile ('Body Parts') von Kleidung und Rüstung nun nicht mehr auf einen angeblich vorhandenen "Rasse"-Eintrag
  * [[#5731](https://gitlab.com/OpenMW/openmw/-/issues/5731)] Röcke, die von NPCs getragen werden, werden nun in der 3D-Ansicht korrekt dargestellt
* *von unelsson*
  * [[#4357](https://gitlab.com/OpenMW/openmw/-/issues/4357)] Die Sortierfunktion innerhalb der 'Journal Infos'- und 'Topic Infos'-Tabellen ist nun deaktiviert; die Reihenfolge der Einträge kann manuell geändert werden
  * [[#4363](https://gitlab.com/OpenMW/openmw/-/issues/4363)] Die Kopierfunkion für 'Journal Infos'- und 'Topic Infos'-Einträge erlaubt es nun, die 'ID' des Eintrags zu bearbeiten
  * [[#5675](https://gitlab.com/OpenMW/openmw/-/issues/5675)] Instanzen (Objekte in der Spielwelt) werden nun mit dem richtigen Master-Index geladen und gespeichert, damit überschriebene Objekte nicht im Spiel geladen werden
  * [[#5703](https://gitlab.com/OpenMW/openmw/-/issues/5703)] Kein Flackern und keine Abstürze mehr auf XFCE-Desktop-Umgebungen
  * [[#5713](https://gitlab.com/OpenMW/openmw/-/issues/5713)] 'Collada'-Modelle werden nun korrekt dargestellt
  * [[#6235](https://gitlab.com/OpenMW/openmw/-/issues/6235)] OpenMW-CS stürzt nun nicht mehr ab, wenn Änderungen am Höhenprofil des Terrains erst rückgängig gemacht und dann wiederholt werden ("undo"/"redo")
* *von Yoae*
  * [[#5384](https://gitlab.com/OpenMW/openmw/-/issues/5384)] Das Löschen von Instanzen in der 3D-Ansicht spiegelt sich nun unmittelbar in allen aktiven 3D-Ansichten wider

**Sonstiges:**
* *von akortunov*
  * [[#5026](https://gitlab.com/OpenMW/openmw/-/issues/5026)] Wettlaufsituationen ("data race") bei der Verwendung von 'rain intensity uniforms' behoben
  * [[#5480](https://gitlab.com/OpenMW/openmw/-/issues/5480)] Qt4 wird nun nicht mehr unterstützt: neue Mindestversion: Qt 5.12
* *von AnyOldName3*
  * [[#4765](https://gitlab.com/OpenMW/openmw/-/issues/4765)] OSG-Arrays können nun nicht mehr von unterschiedlichen Threads in OpenMWs 'ChunkManager' gebunden werden
  * [[#5551](https://gitlab.com/OpenMW/openmw/-/issues/5551)] Windows: OpenMW erzwingt nun keinen Neustart mehr während der Installation
  * [[#5904](https://gitlab.com/OpenMW/openmw/-/issues/5904)] Mesa: OpenSceneGraph-Problem bzgl. RTT-Methode ("render-to-texture") behoben
* *von CedricMocquillon*
  * [[#5520](https://gitlab.com/OpenMW/openmw/-/issues/5520)] Verbesserte Behandlung des Kombinationsfeldes 'Start default character at' ("Starte mit Standard-Charakter in") im Launcher, um Warnmeldungen und Speicherlecks zu verhindern
* *von fr3dz10*
  * [[#5980](https://gitlab.com/OpenMW/openmw/-/issues/5980)] Während der Konfiguration von OpenMW wird nun geprüft, ob 'Bullet' (OpenMWs Physik-Engine) mit doppelter Präzision verwendet wird, und diese Option notfalls erzwungen *[Zusatz von akortunov]*
* *von glebm*
  * [[#5807](https://gitlab.com/OpenMW/openmw/-/issues/5807)] Absturz auf ARM-Systemen behoben, der durch falsche Verarbeitung der Frame-Allokation im 'osg-ffmpeg-videoplayer' verursacht wurde
  * [[#5897](https://gitlab.com/OpenMW/openmw/-/issues/5897)] 'MyGUI' aktualisiert, um Fehler bei dessen dynamischer Verlinkung zu verhindern