﻿1
00:00:09.967 --> 00:00:14.033
Hello, and welcome to another OpenMW release video.

2
00:00:14.733 --> 00:00:20.367
OpenMW is a free and open-source game engine
which runs The Elder Scrolls III: Morrowind

3
00:00:20.567 --> 00:00:24.300
and allows you to enjoy this classic RPG on a modern,

4
00:00:24.500 --> 00:00:27.300
flexible, and continuously updated engine

5
00:00:27.500 --> 00:00:33.333
with native support for macOS, Linux, and Windows.

6
00:00:34.033 --> 00:00:40.450
We, the OpenMW team, are proud to announce
the release of version 0.47.0 of our engine.

7
00:00:40.650 --> 00:00:43.333
Grab it from our download page on openmw.org

8
00:00:43.533 --> 00:00:48.867
or head over to our GitLab repository
to build OpenMW from source.

9
00:00:49.567 --> 00:00:55.050
And now, let's have a look at the main features
of the new OpenMW release;

10
00:00:55.250 --> 00:00:59.050
roll VT!

11
00:01:23.417 --> 00:01:26.833
The most noticeable change
in the new version is object paging,

12
00:01:27.033 --> 00:01:30.967
which makes OpenMW display
distant objects with merged geometry

13
00:01:31.167 --> 00:01:33.383
to reduce the number of draw calls.

14
00:01:33.583 --> 00:01:40.467
This allows you to crank up your viewing distance
without killing your FPS.

15
00:01:45.650 --> 00:01:49.400
Just enable "distant land" in the launcher
and enjoy distant vistas

16
00:01:49.600 --> 00:01:55.550
filled with trees, buildings, and other objects.

17
00:01:59.733 --> 00:02:04.050
Please note that our implementation of object paging
works out of the box.

18
00:02:04.250 --> 00:02:05.467
It’s also very efficient,

19
00:02:05.667 --> 00:02:11.600
even for greater view distances and object densities.

20
00:02:22.017 --> 00:02:26.483
The second feature we want to talk about
is proper groundcover support.

21
00:02:27.183 --> 00:02:32.283
In the past, OpenMW did not differentiate
between normal objects and groundcover

22
00:02:32.483 --> 00:02:37.833
which caused performance and collision issues.

23
00:02:45.983 --> 00:02:50.717
In version 0.47.0, you can specify
groundcover mods in your configuration file

24
00:02:50.917 --> 00:02:55.417
to make them work as expected.

25
00:03:02.100 --> 00:03:06.767
Groundcover mods like "Remiros' Groundcover",
"Aesthesia Groundcover",

26
00:03:06.967 --> 00:03:09.700
or the built-in groundcover
in "Skyrim: Home of the Nords"

27
00:03:09.900 --> 00:03:11.483
-- which you are currently looking at --

28
00:03:11.683 --> 00:03:14.500
will greatly benefit from this change.

29
00:03:15.200 --> 00:03:18.633
However, we are likely going
to implement a more modern, flexible,

30
00:03:18.833 --> 00:03:24.283
and highly efficient groundcover system in the future.

31
00:03:38.800 --> 00:03:42.550
For the new release, we have upgraded
our lighting system to use shaders

32
00:03:42.767 --> 00:03:45.733
instead of a fixed-function pipeline.

33
00:03:46.433 --> 00:03:50.800
This allows OpenMW to display
a huge amount of light sources at the same time

34
00:03:51.000 --> 00:03:53.150
compared to the previous limit of eight

35
00:03:53.367 --> 00:03:56.383
-- which even Skyrim suffers from to this very day.

36
00:03:56.583 --> 00:03:58.033
It also affects groundcover

37
00:03:58.233 --> 00:04:04.467
which vastly improves
the visual quality of nighttime scenes.

38
00:04:08.967 --> 00:04:12.833
However, users of integrated graphics cards
or with older hardware

39
00:04:13.033 --> 00:04:15.617
might need to stick with
"shaders compatibility" mode

40
00:04:15.817 --> 00:04:19.133
which is visually indistinguishable
from regular "shaders" mode,

41
00:04:19.333 --> 00:04:23.067
but has a bigger performance impact
for greater light limits.

42
00:04:23.267 --> 00:04:29.883
Currently, macOS users are restricted to this mode as well.

43
00:04:34.533 --> 00:04:38.083
Please note that, due to technical reasons, some scenes

44
00:04:38.283 --> 00:04:40.067
-- especially interior ones --

45
00:04:40.267 --> 00:04:43.400
might look darker than before in OpenMW.

46
00:04:43.617 --> 00:04:44.800
To alleviate that issue,

47
00:04:45.017 --> 00:04:52.450
we have implemented an in-game slider
for adjusting the minimum interior brightness.

48
00:04:59.983 --> 00:05:03.167
The new version also comes with
several improvements to camera,

49
00:05:03.367 --> 00:05:05.917
actor movement, and physics.

50
00:05:06.617 --> 00:05:09.100
Experience a camera with modern,
highly customisable,

51
00:05:09.300 --> 00:05:11.583
optional features such as:

52
00:05:11.783 --> 00:05:16.383
head-bobbing in first-person view,

53
00:05:17.083 --> 00:05:22.983
a dynamic over-the-shoulder camera
in third-person view,

54
00:05:35.117 --> 00:05:39.917
and an improved vanity camera.

55
00:05:49.017 --> 00:05:52.633
Enable "smooth movement"
and "turn to movement direction" for your character

56
00:05:52.833 --> 00:05:57.567
to make the most out of
Morrowind's very elaborate animations.

57
00:05:58.267 --> 00:06:01.100
Watch other NPCs profit from this change too

58
00:06:01.300 --> 00:06:04.183
-- and marvel at their improved manners:

59
00:06:04.383 --> 00:06:05.883
they avoid collisions,

60
00:06:06.083 --> 00:06:11.617
turn more naturally toward your character
when greeting them,

61
00:06:16.133 --> 00:06:21.433
and even make space for each other in combat.

62
00:06:25.033 --> 00:06:29.350
Last but not least,
OpenMW received various physics improvements,

63
00:06:29.550 --> 00:06:31.800
now supports multi-threaded physics,

64
00:06:32.000 --> 00:06:33.517
and uses a new movement solver

65
00:06:33.717 --> 00:06:36.250
in order to overcome previous performance bottlenecks

66
00:06:36.450 --> 00:06:42.283
and eliminate many physics-related glitches and bugs.

67
00:07:10.683 --> 00:07:14.217
Our last engine feature for today
is a GUI-related change:

68
00:07:14.417 --> 00:07:16.600
dialogue topics now have different colours

69
00:07:16.800 --> 00:07:23.100
depending on whether they provide
new information or not.

70
00:07:33.333 --> 00:07:40.167
This makes talking to
all these hobby encyclopedists a lot easier.

71
00:07:46.317 --> 00:07:53.617
For your convenience,
the colour scheme is customisable in the user settings.

72
00:08:07.133 --> 00:08:11.150
We want to end this list with
two more general points of interest:

73
00:08:11.850 --> 00:08:16.333
First of all, OpenMW comes with its own editor, OpenMW-CS,

74
00:08:16.533 --> 00:08:19.583
which will eventually supersede
Morrowind's original construction set

75
00:08:19.783 --> 00:08:24.317
in terms of functionality and user friendliness.

76
00:08:25.017 --> 00:08:29.450
OpenMW-CS isn't fully feature-complete yet
and it still receives active development

77
00:08:29.667 --> 00:08:33.533
with several new features making it
into the current release, e.g.,

78
00:08:33.733 --> 00:08:35.550
instance drag selection,

79
00:08:35.750 --> 00:08:37.783
improved colours in the 3D view,

80
00:08:37.983 --> 00:08:40.133
and better handling of deleted references,

81
00:08:40.333 --> 00:08:45.267
alongside various bugfixes.

82
00:08:55.067 --> 00:08:59.950
Secondly, OpenMW will eventually allow
content creators to develop stand-alone games,

83
00:09:00.150 --> 00:09:03.400
completely independent from Morrowind's contents.

84
00:09:04.100 --> 00:09:05.450
To facilitate this process,

85
00:09:05.650 --> 00:09:08.583
we are going to provide a setup with
the minimum amount of assets necessary

86
00:09:08.783 --> 00:09:14.283
for game developers to jump right into
working on their own game.

87
00:09:14.983 --> 00:09:20.683
This so-called Game Template will be accompanied by
one or more so-called Example Suites

88
00:09:20.883 --> 00:09:25.533
which are very barebone miniature games demonstrating
certain aspects of the OpenMW engine

89
00:09:25.733 --> 00:09:30.400
and providing a small set of assets themselves.

90
00:09:31.100 --> 00:09:34.617
One of these Example Suite projects
has recently seen great progress

91
00:09:34.817 --> 00:09:40.133
and is about to have its first major release.

92
00:09:44.567 --> 00:09:51.683
A first glimpse at the possibilities
OpenMW will offer in the future.

93
00:10:02.217 --> 00:10:03.733
That's it for this video.

94
00:10:03.933 --> 00:10:04.950
If you want to know more,

95
00:10:05.150 --> 00:10:08.733
consider reading the full release annoucenment
for more in-depth information

96
00:10:08.933 --> 00:10:13.233
and check out our documentation
on GitLab and readthedocs.io.

97
00:10:13.433 --> 00:10:17.300
-- All links are available in the video description.

98
00:10:18.000 --> 00:10:20.550
If you want to join our community, need some help,

99
00:10:20.750 --> 00:10:22.250
or want to give us feedback,

100
00:10:22.450 --> 00:10:26.450
visit our forum on openmw.org,
our community on Reddit,

101
00:10:26.650 --> 00:10:29.350
or our OpenMW Discord server.

102
00:10:30.050 --> 00:10:34.933
For reporting bugs or creating feature requests,
GitLab is your first choice.

103
00:10:35.633 --> 00:10:37.083
And remember:

104
00:10:37.283 --> 00:10:39.367
OpenMW is a volunteer project

105
00:10:39.567 --> 00:10:43.333
which is why we are excited about
each and every new member

106
00:10:43.533 --> 00:10:44.800
-- whether you are a developer,

107
00:10:45.000 --> 00:10:46.500
documentation writer,

108
00:10:46.700 --> 00:10:48.083
PR supporter,

109
00:10:48.283 --> 00:10:50.100
OpenMW-friendly mod creator,

110
00:10:50.300 --> 00:10:51.400
playtester,

111
00:10:51.600 --> 00:10:54.450
or just an average internet lurker.

112
00:10:54.650 --> 00:10:58.850
Thank you for your invaluable support!

113
00:10:59.567 --> 00:11:04.583
We hope to see you again for the upcoming
0.48.0 release announcement

114
00:11:04.783 --> 00:11:09.183
-- or any video content
we can churn out in the meantime.

115
00:11:09.883 --> 00:11:16.383
Until then; thanks for watching!

116
00:11:47.750 --> 00:11:51.550
Lua...

117
00:11:51.750 --> 00:11:56.967
It's coming...

118
00:11:57.167 --> 00:12:03.917
48...

