[[_TOC_]]

## I   Intro

#### 1   Greeting & Introductory Clause
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>Hello, and welcome to another OpenMW release video.</td>
<td><tt>0-Intro_1_Greeting_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>OpenMW is a free and open-source game engine which runs The Elder Scrolls III: Morrowind and allows you to enjoy this classic RPG on a modern, flexible, and continuously updated engine with native support for macOS, Linux, and Windows.</td>
<td><tt>0-Intro_1_Greeting_02.wav</tt></td>
</tr>
</table>

#### 2   Announcement
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>We, the OpenMW team, are proud to announce the release of version 0.47.0 of our engine. Grab it from our download page on openmw.org or head over to our GitLab repository to build OpenMW from source.</td>
<td><tt>0-Intro_2_Announcement.wav</tt></td>
</tr>
</table>

#### 3   Roll Credits!
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>And now, let's have a look at the main features of the new OpenMW release; roll VT!</td>
<td><tt>0-Intro_3_Roll-Credits.wav</tt></td>
</tr>
</table>

#### 4   Credits

#### 5   Special Thanks

## II   Main Part

#### 1   Object Paging
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>The most noticeable change in the new version is object paging, which makes OpenMW display distant objects with merged geometry to reduce the number of draw calls. This allows you to crank up your viewing distance without killing your FPS.</td>
<td><tt>1-Main-Part_1_Object-Paging_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Just enable "distant land" in the launcher and enjoy distant vistas filled with trees, buildings, and other objects.</td>
<td><tt>1-Main-Part_1_Object-Paging_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Please note that our implementation of object paging works out of the box. It’s also very efficient, even for greater view distances and object densities.</td>
<td><tt>1-Main-Part_1_Object-Paging_03.wav</tt></td>
</tr>
</table>

#### 2   Groundcover
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>The second feature we want to talk about is proper groundcover support.</td>
<td><tt>1-Main-Part_2_Groundcover_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>In the past, OpenMW did not differentiate between normal objects and groundcover which caused performance and collision issues.</td>
<td><tt>1-Main-Part_2_Groundcover_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>In version 0.47.0, you can specify groundcover mods in your configuration file to make them work as expected.</td>
<td><tt>1-Main-Part_2_Groundcover_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Groundcover mods like "Remiros' Groundcover", "Aesthesia Groundcover", or the built-in groundcover in "Skyrim: Home of the Nords" -- which you are currently looking at -- will greatly benefit from this change. However, we are likely going to implement a more modern, flexible, and highly efficient groundcover system in the future.</td>
<td><tt>1-Main-Part_2_Groundcover_04.wav</tt></td>
</tr>
</table>

#### 3   Light Rendering
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>For the new release, we have upgraded our lighting system to use shaders instead of a fixed-function pipeline.</td>
<td><tt>1-Main-Part_3_Light-Rendering_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>This allows OpenMW to display a huge amount of light sources at the same time compared to the previous limit of eight -- which even Skyrim suffers from to this very day. It also affects groundcover which vastly improves the visual quality of nighttime scenes.</td>
<td><tt>1-Main-Part_3_Light-Rendering_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>However, users of integrated graphics cards or with older hardware might need to stick with "shaders compatibility" mode which is visually indistinguishable from regular "shaders" mode, but has a bigger performance impact for greater light limits. Currently, macOS users are restricted to this mode as well.</td>
<td><tt>1-Main-Part_3_Light-Rendering_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Please note that, due to technical reasons, some scenes -- especially interior ones -- might look darker than before in OpenMW. To alleviate that issue, we have implemented an in-game slider for adjusting the minimum interior brightness.</td>
<td><tt>1-Main-Part_3_Light-Rendering_04.wav</tt></td>
</tr>
</table>

#### 4   Camera, Actor Movement & Physics
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>The new version also comes with several improvements to camera, actor movement, and physics.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Experience a camera with modern, highly customisable, optional features such as: head-bobbing in first-person view, a dynamic over-the-shoulder camera in third-person view, and an improved vanity camera.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_02.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Enable "smooth movement" and "turn to movement direction" for your character to make the most out of Morrowind's *very* elaborate animations. Watch other NPCs profit from this change too -- and marvel at their improved manners: they avoid collisions, turn more naturally toward your character when greeting them, and even make space for each other in combat.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_03.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Last but not least, OpenMW received various physics improvements, now supports multi-threaded physics, and uses a new movement solver in order to overcome previous performance bottlenecks and eliminate many physics-related glitches and bugs.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_04.wav</tt></td>
</tr>
</table>

#### 5   Greyed-Out Dialogue Topics
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>Our last engine feature for today is a GUI-related change: dialogue topics now have different colours depending on whether they provide new information or not.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>This makes talking to all these hobby encyclopedists a lot easier.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>For your convenience, the colour scheme is customisable in the user settings.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_03.wav</tt></td>
</tr>
</table>

#### 6   OpenMW-CS & Example Suite
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>We want to end this list with two more general points of interest:</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>First of all, OpenMW comes with its own editor, OpenMW-CS, which will eventually supersede Morrowind's original construction set in terms of functionality and user friendliness.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>OpenMW-CS isn't fully feature-complete yet and it still receives active development with several new features making it into the current release, e.g., instance drag selection, improved colours in the 3D view, and better handling of deleted references, alongside various bugfixes.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Secondly, OpenMW will eventually allow content creators to develop stand-alone games, completely independent from Morrowind's contents.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_04.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>To facilitate this process, we are going to provide a setup with the minimum amount of assets necessary for game developers to jump right into working on their own game.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_05.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>This so-called Game Template will be accompanied by one or more so-called Example Suites which are very barebone miniature games demonstrating certain aspects of the OpenMW engine and providing a small set of assets themselves.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_06.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>One of these Example Suite projects has recently seen great progress and is about to have its first major release.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_07.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>A first glimpse at the possibilities OpenMW will offer in the future.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_08.wav</tt></td>
</tr>
</table>

## III   Outro

#### 1   Release Info
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>That's it for this video. If you want to know more, consider reading the full release annoucenment for more in-depth information and check out our documentation on GitLab and readthedocs.io. -- All links are available in the video description.</td>
<td><tt>2-Outro_1_Release-Info.wav</tt></td>
</tr>
</table>

#### 2   Recruitment
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>If you want to join our community, need some help, or want to give us feedback, visit our forum on openmw.org, our community on Reddit, or our OpenMW Discord server.</td>
<td><tt>2-Outro_2_Recruitment_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>For reporting bugs or creating feature requests, GitLab is your first choice.</td>
<td><tt>2-Outro_2_Recruitment_02.wav</tt></td>
</tr>
<tr>
<td></td>
<td>And remember: OpenMW is a volunteer project which is why we are excited about each and every new member -- whether you are a developer, documentation writer, PR supporter, OpenMW-friendly mod creator, playtester, or just an average internet lurker.</td>
<td><tt>2-Outro_2_Recruitment_03.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Thank you for your invaluable support!</td>
<td><tt>2-Outro_2_Recruitment_04.wav</tt></td>
</tr>
</table>

#### 3   Farewell
<table>
<tr>
<td><b><tt>[Hri]</tt></b></td>
<td>We hope to see you again for the upcoming 0.48.0 release announcement -- or any video content we can churn out in the meantime.</td>
<td><tt>2-Outro_3_Farewell_01.wav</tt></td>
</tr>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Until then; thanks for watching!</td>
<td><tt>2-Outro_3_Farewell_02.wav</tt></td>
</tr>
</table>
