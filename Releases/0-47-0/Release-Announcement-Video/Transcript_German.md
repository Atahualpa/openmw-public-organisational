[[_TOC_]]

## I   Intro

#### 1   Greeting & Introductory Clause
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Hallo, und herzlich willkommen zum ersten OpenMW-Video in deutscher Sprache!</td>
<td><tt>0-Intro_1_Greeting_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>OpenMW ist eine freie Open-Source-Engine, die es Euch erlaubt, den Rollenspielklassiker "The Elder Scrolls III: Morrowind" auf einer modernen, flexiblen und stets auf dem neuesten Stand gehaltenen Spiele-Engine zu genießen - und das von Hause aus auf Windows, macOS oder Linux.</td>
<td><tt>0-Intro_1_Greeting_02.wav</tt></td>
</tr>
</table>

#### 2   Announcement
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Wir, das OpenMW-Team, sind stolz darauf, die Veröffentlichung der Version 0.47.0 unserer Engine verkünden zu dürfen. Ladet sie auf unserer Homepage, openmw.org, herunter oder geht auf unsere GitLab-Projektseite und erstellt Euch OpenMW direkt aus den Quelldaten. Beachtet bitte, dass alle Links auf englischsprachige Seiten führen und auch der Spiele-Launcher und Teile der Spieleinstellungen auf Englisch sind. Nichtsdestotrotz könnt ihr OpenMW für eine deutsche Morrowind-Version verwenden und dann wie gewohnt auf Deutsch spielen.</td>
<td><tt>0-Intro_2_Announcement.wav</tt></td>
</tr>
</table>

#### 3   Roll Credits!
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Und nun zu den Hauptfeatures von OpenMW, Version 0.47.0; Film ab!</td>
<td><tt>0-Intro_3_Roll-Credits.wav</tt></td>
</tr>
</table>

#### 4   Credits

#### 5   Special Thanks

## II   Main Part

#### 1   Object Paging
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Die offensichtlichste Änderung in der neuen Version ist das so genannte Objekt-Paging, das es OpenMW erlaubt, entfernte Objekte zu verschmelzen, um so die Anzahl der Zeichenaufrufe an die Grafikkarte zu verringern. Damit könnt Ihr Eure Sichtweite hochschrauben, ohne dass die Bildwiederholrate in den Keller geht.</td>
<td><tt>1-Main-Part_1_Object-Paging_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Aktiviert einfach die Option "distant land" im OpenMW-Launcher und genießt eindrucksvolle Panoramen voller Bäume, Gebäude und anderer Details.</td>
<td><tt>1-Main-Part_1_Object-Paging_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Beachtet bitte, dass unsere Version von Objekt-Paging nach Aktivierung in den Einstellungen ohne Weiteres funktioniert. Sie ist außerdem sehr effizient, auch für größere Sichtweiten und Objektdichten.</td>
<td><tt>1-Main-Part_1_Object-Paging_03.wav</tt></td>
</tr>
</table>

#### 2   Groundcover
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Das zweite Feature, über das wir reden wollen, ist die korrekte Handhabung von Bodenvegetation, z.B. Grasobjekten aus entsprechenden Mods.</td>
<td><tt>1-Main-Part_2_Groundcover_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Frühere OpenMW-Versionen konnten nicht zwischen normalen Objekten und Bodenvegation unterscheiden, was Kollisions- und Performance-Probleme zur Folge hatte.</td>
<td><tt>1-Main-Part_2_Groundcover_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>In der neuen Version könnt Ihr Mods mit Bodenvegetation gesondert in Eurer Konfigurationsdatei angeben, damit sie ordentlich funktionieren.</td>
<td><tt>1-Main-Part_2_Groundcover_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Mods wie "Remiros' Groundcover", "Aesthesia Groundcover" oder die momentan gezeigte Bodenvegetation in "Skyrim: Home of the Nords" profitieren stark von dieser Änderung. Allerdings werden wir höchstwahrscheinlich in nicht allzu ferner Zukunft ein moderneres, flexibleres und hocheffizientes System zur Darstellung von Bodenvegetation implementieren.</td>
<td><tt>1-Main-Part_2_Groundcover_04.wav</tt></td>
</tr>
</table>

#### 3   Light Rendering
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Für die aktuelle Veröffentlichung haben wir OpenMWs Beleuchtungssystem überarbeitet: Es nutzt nun Shader statt der technisch veralteten "Fixed-Function-Pipeline".</td>
<td><tt>1-Main-Part_3_Light-Rendering_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Das erlaubt es OpenMW, Licht aus einer großen Anzahl unterschiedlicher Quellen gleichzeitig auf einem Objekt darzustellen - verglichen mit der vorherigen Beschränkung auf acht Quellen, die sogar noch in Skyrim vorhanden ist. Auch die vorhin erwähnte Bodenvegetation wird nun von Lichtquellen beleuchtet, was gerade nächtliche Szenerien enorm aufwertet.</td>
<td><tt>1-Main-Part_3_Light-Rendering_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Leider müssen Spieler mit integrierten Grafikkarten oder älterer Hardware möglicherweise auf den "shaders compatibility"-Modus zurückgreifen, der zwar optisch nicht vom normalen "shaders"-Modus zu unterscheiden ist, aber bei größeren Lichtquellen-Grenzwerten eine schlechtere Performance aufweist. Momentan sind auch macOS-Nutzer auf diesen Modus angewiesen.</td>
<td><tt>1-Main-Part_3_Light-Rendering_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Beachtet bitte außerdem, dass einige Szenen - v.a. in Innenräumen - aus technischen Gründen dunkler als in früheren Versionen sind. Um dieses Problem zu umgehen, haben wir einen Schieberegler in den Spieloptionen eingefügt, mit dem Ihr die Mindesthelligkeit in Innenräumen festlegen könnt.</td>
<td><tt>1-Main-Part_3_Light-Rendering_04.wav</tt></td>
</tr>
</table>

#### 4   Camera, Actor Movement & Physics
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Version 0.47.0 beinhaltet auch viele Verbesserungen für Kameraverhalten, Charakteranimationen und Spielphysik.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Erlebt eine Kameraführung mit modernen, konfigurierbaren und vollständig optionalen Vorzügen wie z.B.: Schwanken der Kamera in der Egoperspektive, einer dynamischen Über-die-Schulter-Ansicht in der Verfolgerperspektive und einer verbesserten freien Heldenansicht.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_02.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Aktiviert "smooth movement" und "turn to movement direction" im Launcher, um für Euren Helden das beste aus Morrowinds ausgeklügelten Animationen zu machen. Seht wie NPCs ebenfalls von dieser Änderung profitieren - und bewundert ihre verbesserten Manieren: sie vermeiden Zusammenstöße, wenden sich Eurem Helden zur Begrüßung weniger verkrampft zu und machen sich im Kampf gegenseitig Platz.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_03.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Zu guter Letzt hat OpenMWs Spielphysik diverse Verbesserungen erhalten, unterstützt nun Multi-Threading und nutzt einen neuen Wegfindung-Algorithmus, um Performance-Engpässe sowie jede Menge Bugs und Glitches zu beseitigen.</td>
<td><tt>1-Main-Part_4_Camera-Actor-Movement-and-Physics_04.wav</tt></td>
</tr>
</table>

#### 5   Greyed-Out Dialogue Topics
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Die letzte Neuerung auf Seiten der Engine für heute bezieht sich auf die Benutzeroberfläche: Gesprächsoptionen werden nun abhängig davon, ob sie neue Informationen bringen oder nicht, unterschiedlich eingefärbt.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Das macht es wesentlich einfacher, mit all diesen wandelnden Lexika zu kommunizieren.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Das verwendete Farbschema könnt Ihr manuell in Eurer lokalen Einstellungsdatei anpassen.</td>
<td><tt>1-Main-Part_5_Greyed-Out-Dialogue-Topics_03.wav</tt></td>
</tr>
</table>

#### 6   OpenMW-CS & Example Suite
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Zum Schluss wollen wir noch zwei allgemeine Punkte ansprechen:</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_01.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Erstens enthält OpenMW einen eigenen Editor, OpenMW-CS, der in absehbarer Zeit Morrowinds ursprüngliches "Construction Set" in Sachen Funktionalität und Bedienkomfort überflügeln soll.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_02.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Auch Version 0.47.0 beinhaltet wieder einige neue Features für unseren Editor, z.B. Werkzeuge zur Auswahl mehrerer Objekte und verbesserte Hintergrundfarben in der 3D-Ansicht sowie die bessere Handhabung von gelöschten Objekten. Zusätzlich wurden wieder diverse Bugs behoben.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_03.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Zweitens ist es unser erklärtes Ziel, dass OpenMW Entwicklern ermöglichen soll, eigenständige, von Morrowind unabhängige Spiele für unsere Engine zu erschaffen.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_04.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Zu diesem Zweck werden wir eine minimale Installation mit allen von Entwicklern benötigten Spielressourcen bereitstellen, damit diese sich sofort auf die eigentliche Spieleentwicklung konzentrieren können.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_05.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Diese so genannte Spiel-Vorlage - "Game Template" - wird von einer oder mehreren so genannten Beispiel-Suiten - "Example Suites" - ergänzt, die ihrerseits eigenständige kleine Spiele in der OpenMW-Engine sind und zusätzliche Spielressourcen zur Verfügung stellen.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_06.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Eine dieser Beispiel-Suiten hat in letzter Zeit große Fortschritte gemacht und wird demnächst ihren ersten großen Release feiern.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_07.wav</tt></td>
</tr>
<tr>
<td><b><tt></tt></b></td>
<td>Ein erster Blick auf die großartigen Möglichkeiten, die OpenMW in Zukunft bieten wird.</td>
<td><tt>1-Main-Part_6_OpenMW-CS-and-Example-Suite_08.wav</tt></td>
</tr>
</table>

## III   Outro

#### 1   Release Info
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>So, das war's erstmal! Falls Ihr noch mehr zu den Einzelheiten dieses Releases wissen wollt, könnt Ihr Euch die offizielle Release-Ankündigung durchlesen, die es auch auf Deutsch gibt. Für Details zur Installation und zu den Spieleinstellungen müsst Ihr auf die englische Dokumentation auf GitLab oder readthedocs.io zurückgreifen.</td>
<td><tt>2-Outro_1_Release-Info.wav</tt></td>
</tr>
</table>

#### 2   Recruitment
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Falls Ihr unserer Gemeinschaft beitreten wollt, technische Hilfe benötigt oder uns Feedback geben wollt, besucht unser Forum auf openmw.org, unsere Community auf Reddit oder unseren OpenMW-Discord-Server. Wir haben auch einen deutschsprachigen Discord-Kanal, so dass Ihr notfalls auch ohne Englischkenntnisse Hilfe erhalten solltet.</td>
<td><tt>2-Outro_2_Recruitment_01.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Zum Melden von Bugs oder zum Vorschlagen von Features ist GitLab das Mittel der Wahl.</td>
<td><tt>2-Outro_2_Recruitment_02.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Und denkt immer daran: OpenMW ist ein Hobby-Projekt, das vollständig auf Freiwilligkeit basiert. Umso mehr freuen wir uns über jedes neue Mitglied -- sei es eine Entwicklerin, jemand, der Quellcode und Einstellungen dokumentiert, jemand, der mit ins PR-Horn stößt, eine OpenMW-interessierte Mod-Erstellerin, ein Testspieler oder der uns allen wohlbekannte stille Beobachter.</td>
<td><tt>2-Outro_2_Recruitment_03.wav</tt></td>
</tr>
<tr>
<td></td>
<td>Vielen Dank für Eure unendlich wertvolle Unterstützung!</td>
<td><tt>2-Outro_2_Recruitment_04.wav</tt></td>
</tr>
</table>

#### 3   Farewell
<table>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Wir hoffen, Euch im Ankündigungsvideo für Version 0.48.0 wiederzusehen.</td>
<td><tt>2-Outro_3_Farewell_01.wav</tt></td>
</tr>
<tr>
<td><b><tt>[Mar]</tt></b></td>
<td>Danke fürs Zuschauen - und gehabt Euch wohl!</td>
<td><tt>2-Outro_3_Farewell_02.wav</tt></td>
</tr>
</table>
