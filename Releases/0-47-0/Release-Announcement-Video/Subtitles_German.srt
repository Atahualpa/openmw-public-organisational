1
00:00:09.800 --> 00:00:14.533
Hallo, und herzlich willkommen
zum ersten OpenMW-Video in deutscher Sprache!

2
00:00:14.733 --> 00:00:17.650
OpenMW ist eine freie Open-Source-Engine,
die es Euch erlaubt,

3
00:00:17.850 --> 00:00:20.850
den Rollenspielklassiker
"The Elder Scrolls III: Morrowind"

4
00:00:21.067 --> 00:00:22.717
auf einer modernen, flexiblen

5
00:00:22.917 --> 00:00:26.250
und stets auf dem neuesten Stand gehaltenen
Spiele-Engine zu genie�en

6
00:00:26.450 --> 00:00:30.300
- und das von Hause aus
auf Windows, macOS oder Linux.

7
00:00:31.000 --> 00:00:33.550
Wir, das OpenMW-Team, sind stolz darauf,

8
00:00:33.750 --> 00:00:38.817
die Ver�ffentlichung der Version 0.47.0
unserer Engine verk�nden zu d�rfen.

9
00:00:39.000 --> 00:00:42.033
Ladet sie auf unserer Homepage, openmw.org, herunter

10
00:00:42.233 --> 00:00:47.333
oder geht auf unsere GitLab-Projektseite
und erstellt Euch OpenMW direkt aus den Quelldaten.

11
00:00:48.033 --> 00:00:51.433
Beachtet bitte, dass alle Links
auf englischsprachige Seiten f�hren

12
00:00:51.633 --> 00:00:55.783
und auch der Spiele-Launcher
und Teile der Spieleinstellungen auf Englisch sind.

13
00:00:55.983 --> 00:00:59.633
Nichtsdestotrotz k�nnt ihr OpenMW
f�r eine deutsche Morrowind-Version verwenden

14
00:00:59.833 --> 00:01:02.817
und dann wie gewohnt auf Deutsch spielen.

15
00:01:03.500 --> 00:01:08.917
Und nun zu den Hauptfeatures
von OpenMW, Version 0.47.0;

16
00:01:09.100 --> 00:01:12.783
Film ab!

17
00:01:37.533 --> 00:01:41.283
Die offensichtlichste �nderung in der neuen Version
ist das so genannte Objekt-Paging,

18
00:01:41.483 --> 00:01:44.600
das es OpenMW erlaubt,
entfernte Objekte zu verschmelzen,

19
00:01:44.800 --> 00:01:48.233
um so die Anzahl der Zeichenaufrufe
an die Grafikkarte zu verringern.

20
00:01:48.933 --> 00:01:50.700
Damit k�nnt Ihr Eure Sichtweite hochschrauben,

21
00:01:50.900 --> 00:01:55.950
ohne dass die Bildwiederholrate in den Keller geht.

22
00:02:00.033 --> 00:02:03.967
Aktiviert einfach die Option "distant land"
im OpenMW-Launcher

23
00:02:04.167 --> 00:02:11.767
und genie�t eindrucksvolle Panoramen
voller B�ume, Geb�ude und anderer Details.

24
00:02:14.033 --> 00:02:16.383
Beachtet bitte,
dass unsere Version von Objekt-Paging

25
00:02:16.583 --> 00:02:20.350
nach Aktivierung in den Einstellungen
ohne Weiteres funktioniert.

26
00:02:20.550 --> 00:02:22.017
Sie ist au�erdem sehr effizient,

27
00:02:22.217 --> 00:02:27.450
auch f�r gr��ere Sichtweiten und Objektdichten.

28
00:02:36.050 --> 00:02:37.883
Das zweite Feature, �ber das wir reden wollen,

29
00:02:38.083 --> 00:02:40.450
ist die korrekte Handhabung von Bodenvegetation,

30
00:02:40.650 --> 00:02:43.317
z.B. Grasobjekten aus entsprechenden Mods.

31
00:02:44.017 --> 00:02:48.883
Fr�here OpenMW-Versionen konnten nicht zwischen
normalen Objekten und Bodenvegation unterscheiden,

32
00:02:49.083 --> 00:02:54.633
was Kollisions- und Performance-
Probleme zur Folge hatte.

33
00:03:00.033 --> 00:03:04.900
In der neuen Version k�nnt Ihr Mods mit Bodenvegetation
gesondert in Eurer Konfigurationsdatei angeben,

34
00:03:05.117 --> 00:03:09.633
damit sie ordentlich funktionieren.

35
00:03:17.033 --> 00:03:21.333
Mods wie "Remiros' Groundcover",
"Aesthesia Groundcover"

36
00:03:21.533 --> 00:03:24.983
oder die momentan gezeigte Bodenvegetation
in "Skyrim: Home of the Nords"

37
00:03:25.183 --> 00:03:27.517
profitieren stark von dieser �nderung.

38
00:03:28.217 --> 00:03:31.550
Allerdings werden wir h�chstwahrscheinlich
in nicht allzu ferner Zukunft ein moderneres,

39
00:03:31.750 --> 00:03:39.217
flexibleres und hocheffizientes System
zur Darstellung von Bodenvegetation implementieren.

40
00:03:53.033 --> 00:03:57.483
F�r die aktuelle Ver�ffentlichung haben wir
OpenMWs Beleuchtungssystem �berarbeitet:

41
00:03:58.183 --> 00:04:02.333
Es nutzt nun Shader statt der technisch
veralteten "Fixed-Function-Pipeline".

42
00:04:02.533 --> 00:04:03.700
Das erlaubt es OpenMW,

43
00:04:03.900 --> 00:04:08.600
Licht aus einer gro�en Anzahl unterschiedlicher Quellen
gleichzeitig auf einem Objekt darzustellen

44
00:04:08.800 --> 00:04:11.150
- verglichen mit der vorherigen Beschr�nkung
auf acht Quellen,

45
00:04:11.350 --> 00:04:13.950
die sogar noch in Skyrim vorhanden ist.

46
00:04:14.650 --> 00:04:18.133
Auch die vorhin erw�hnte Bodenvegetation
wird nun von Lichtquellen beleuchtet,

47
00:04:18.333 --> 00:04:22.350
was gerade n�chtliche Szenerien enorm aufwertet.

48
00:04:23.050 --> 00:04:26.533
Leider m�ssen Spieler mit integrierten Grafikkarten
oder �lterer Hardware

49
00:04:26.733 --> 00:04:30.017
m�glicherweise auf den
"shaders compatibility"-Modus zur�ckgreifen,

50
00:04:30.217 --> 00:04:33.800
der zwar optisch nicht vom normalen
"shaders"-Modus zu unterscheiden ist,

51
00:04:34.000 --> 00:04:38.050
aber bei gr��eren Lichtquellen-Grenzwerten
eine schlechtere Performance aufweist.

52
00:04:38.767 --> 00:04:44.567
Momentan sind auch macOS-Nutzer
auf diesen Modus angewiesen.

53
00:04:49.050 --> 00:04:51.050
Beachtet bitte au�erdem,
dass einige Szenen

54
00:04:51.250 --> 00:04:52.283
- v.a. in Innenr�umen -

55
00:04:52.483 --> 00:04:56.033
aus technischen Gr�nden
dunkler als in fr�heren Versionen sind.

56
00:04:56.233 --> 00:04:59.850
Um dieses Problem zu umgehen,
haben wir einen Schieberegler in den Spieloptionen eingef�gt,

57
00:05:00.050 --> 00:05:05.700
mit dem Ihr die Mindesthelligkeit
in Innenr�umen festlegen k�nnt.

58
00:05:14.033 --> 00:05:17.933
Version 0.47.0 beinhaltet auch
viele Verbesserungen f�r Kameraverhalten,

59
00:05:18.133 --> 00:05:20.833
Charakteranimationen und Spielphysik.

60
00:05:21.033 --> 00:05:23.817
Erlebt eine Kameraf�hrung mit modernen,
konfigurierbaren

61
00:05:24.017 --> 00:05:27.800
und vollst�ndig optionalen Vorz�gen wie z.B.:

62
00:05:28.017 --> 00:05:32.300
Schwanken der Kamera in der Egoperspektive,

63
00:05:33.017 --> 00:05:39.250
einer dynamischen �ber-die-Schulter-Ansicht
in der Verfolgerperspektive

64
00:05:51.017 --> 00:05:56.250
und einer verbesserten freien Heldenansicht.

65
00:06:03.033 --> 00:06:07.383
Aktiviert "smooth movement"
und "turn to movement direction" im Launcher,

66
00:06:07.583 --> 00:06:12.833
um f�r Euren Helden das beste
aus Morrowinds ausgekl�gelten Animationen zu machen.

67
00:06:13.033 --> 00:06:16.183
Seht wie NPCs ebenfalls von dieser �nderung profitieren

68
00:06:16.383 --> 00:06:18.800
- und bewundert ihre verbesserten Manieren:

69
00:06:19.500 --> 00:06:21.800
Sie vermeiden Zusammenst��e,

70
00:06:22.500 --> 00:06:28.200
wenden sich Eurem Helden
zur Begr��ung weniger verkrampft zu

71
00:06:31.983 --> 00:06:37.317
und machen sich im Kampf gegenseitig Platz.

72
00:06:41.067 --> 00:06:44.700
Zu guter Letzt hat OpenMWs Spielphysik
diverse Verbesserungen erhalten,

73
00:06:44.900 --> 00:06:46.417
unterst�tzt nun Multi-Threading

74
00:06:46.617 --> 00:06:48.283
und nutzt einen neuen Wegfindung-Algorithmus,

75
00:06:48.483 --> 00:06:54.950
um Performance-Engp�sse sowie jede Menge
Bugs und Glitches zu beseitigen.

76
00:07:26.500 --> 00:07:31.167
Die letzte Neuerung auf Seiten der Engine f�r heute
bezieht sich auf die Benutzeroberfl�che:

77
00:07:31.867 --> 00:07:40.883
Gespr�chsoptionen werden nun abh�ngig davon, ob sie neue
Informationen bringen oder nicht, unterschiedlich eingef�rbt.

78
00:07:49.000 --> 00:07:55.783
Das macht es wesentlich einfacher,
 mit all diesen wandelnden Lexika zu kommunizieren.

79
00:08:03.000 --> 00:08:10.017
Das verwendete Farbschema k�nnt Ihr
manuell in Eurer lokalen Einstellungsdatei anpassen.

80
00:08:23.000 --> 00:08:26.767
Zum Schluss wollen wir noch
zwei allgemeine Punkte ansprechen:

81
00:08:27.467 --> 00:08:31.900
Erstens enth�lt OpenMW einen eigenen Editor,
OpenMW-CS,

82
00:08:32.100 --> 00:08:35.050
der in absehbarer Zeit
Morrowinds urspr�ngliches "Construction Set"

83
00:08:35.250 --> 00:08:39.300
in Sachen Funktionalit�t und Bedienkomfort �berfl�geln soll.

84
00:08:40.000 --> 00:08:45.000
Auch Version 0.47.0 beinhaltet wieder
einige neue Features f�r unseren Editor,

85
00:08:45.200 --> 00:08:50.450
z.B. Werkzeuge zur Auswahl mehrerer Objekte
und verbesserte Hintergrundfarben in der 3D-Ansicht

86
00:08:50.650 --> 00:08:53.783
sowie die bessere Handhabung von gel�schten Objekten.

87
00:08:54.483 --> 00:09:00.050
Zus�tzlich wurden wieder diverse Bugs behoben.

88
00:09:10.950 --> 00:09:15.167
Zweitens ist es unser erkl�rtes Ziel,
dass OpenMW Entwicklern erm�glichen soll,

89
00:09:15.367 --> 00:09:19.783
eigenst�ndige, von Morrowind unabh�ngige Spiele
f�r unsere Engine zu erschaffen.

90
00:09:20.483 --> 00:09:22.783
Zu diesem Zweck werden wir eine minimale Installation

91
00:09:22.983 --> 00:09:26.033
mit allen von Entwicklern
ben�tigten Spielressourcen bereitstellen,

92
00:09:26.233 --> 00:09:30.300
damit diese sich sofort auf die eigentliche
Spieleentwicklung konzentrieren k�nnen.

93
00:09:31.000 --> 00:09:34.367
Diese so genannte Spiel-Vorlage
- "Game Template" -

94
00:09:34.567 --> 00:09:39.133
wird von einer oder mehreren so genannten Beispiel-Suiten
- "Example Suites" - erg�nzt,

95
00:09:39.333 --> 00:09:42.783
die ihrerseits eigenst�ndige kleine Spiele
in der OpenMW-Engine sind

96
00:09:42.983 --> 00:09:46.317
und zus�tzliche Spielressourcen
zur Verf�gung stellen.

97
00:09:47.017 --> 00:09:50.033
Eine dieser Beispiel-Suiten
hat in letzter Zeit gro�e Fortschritte gemacht

98
00:09:50.233 --> 00:09:55.750
und wird demn�chst
ihren ersten gro�en Release feiern.

99
00:10:00.450 --> 00:10:02.800
Ein erster Blick auf die gro�artigen M�glichkeiten,

100
00:10:03.000 --> 00:10:07.967
die OpenMW in Zukunft bieten wird.

101
00:10:18.017 --> 00:10:20.117
So, das war's erstmal!

102
00:10:20.317 --> 00:10:22.883
Falls Ihr noch mehr zu den Einzelheiten
dieses Releases wissen wollt,

103
00:10:23.083 --> 00:10:25.200
k�nnt Ihr Euch die offizielle
Release-Ank�ndigung durchlesen,

104
00:10:25.400 --> 00:10:27.217
die es auch auf Deutsch gibt.

105
00:10:27.417 --> 00:10:29.950
F�r Details zur Installation
und zu den Spieleinstellungen

106
00:10:30.150 --> 00:10:35.533
m�sst Ihr auf die englische Dokumentation
auf GitLab oder readthedocs.io zur�ckgreifen.

107
00:10:36.233 --> 00:10:38.200
Falls Ihr unserer Gemeinschaft beitreten wollt,

108
00:10:38.400 --> 00:10:41.250
technische Hilfe ben�tigt
oder uns Feedback geben wollt,

109
00:10:41.450 --> 00:10:45.833
besucht unser Forum auf openmw.org,
unsere Community auf Reddit

110
00:10:46.033 --> 00:10:48.850
oder unseren OpenMW-Discord-Server.

111
00:10:49.050 --> 00:10:51.050
Wir haben auch einen
deutschsprachigen Discord-Kanal,

112
00:10:51.250 --> 00:10:54.800
so dass Ihr notfalls auch
ohne Englischkenntnisse Hilfe erhalten solltet.

113
00:10:55.500 --> 00:11:01.283
Zum Melden von Bugs oder zum Vorschlagen von Features
ist GitLab das Mittel der Wahl.

114
00:11:01.983 --> 00:11:03.333
Und denkt immer daran:

115
00:11:03.533 --> 00:11:07.933
OpenMW ist ein Hobby-Projekt,
das vollst�ndig auf Freiwilligkeit basiert.

116
00:11:08.133 --> 00:11:10.350
Umso mehr freuen wir uns
�ber jedes neue Mitglied

117
00:11:10.550 --> 00:11:11.900
- sei es eine Entwicklerin,

118
00:11:12.117 --> 00:11:14.850
jemand, der Quellcode und Einstellungen dokumentiert,

119
00:11:15.050 --> 00:11:17.183
jemand, der mit ins PR-Horn st��t,

120
00:11:17.383 --> 00:11:19.683
eine OpenMW-interessierte Mod-Erstellerin,

121
00:11:19.883 --> 00:11:21.117
ein Testspieler

122
00:11:21.317 --> 00:11:24.250
oder der uns allen wohlbekannte stille Beobachter.

123
00:11:24.450 --> 00:11:28.300
Vielen Dank f�r Eure unendlich wertvolle Unterst�tzung!

124
00:11:29.000 --> 00:11:34.367
Wir hoffen, Euch im Ank�ndigungsvideo
f�r Version 0.48.0 wiederzusehen.

125
00:11:35.050 --> 00:11:41.417
Danke f�rs Zuschauen
- und gehabt Euch wohl!

126
00:12:09.333 --> 00:12:13.750
Lua...

127
00:12:13.950 --> 00:12:20.400
Seht Euch vor...

128
00:12:20.600 --> 00:12:26.900
48...